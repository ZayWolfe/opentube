(function (window, document) {
    
    function start (){
        console.log("started");
        
        document.getElementById("files").addEventListener("change",
        function (evt){
            var files = evt.target.files;
            
            for( var i = 0, f; f = files[i]; i++){
                console.log(f.type);
                
                if( f.type.match("video.*") ){
                    var reader = new FileReader();
                    
                    /*reader.onload = function (evt2){
                        document.getElementById("stage").innerHTML = "" +
                        '<video width="400px"  controls>' +
                        '<source src="' + evt2.target.result + '" type="video/mp4">' +
                        '</video>'
                    }
                    
                    reader.readAsDataURL(f);/**/
                    
                    reader.onload = function(evt2){
                       var player = document.createElement("video");
                       player.width = 400;
                       player.controls = true;
                       player.src = "data:video/mp4;base64," + btoa(evt2.target.result);
                       document.getElementById("stage").appendChild(player);
                       player.load();
                        
                        /*document.getElementById("stage").innerHTML = "" +
                        '<video width="400px"  controls>' +
                        '<source src=" "data:video/mp4;base64,"' + btoa(evt2.target.result) + '" type="video/mp4">' +
                        '</video>'*/
                    }
                    
                    reader.onProgress = function(evt2){
                        console.log(evt2.loaded);
                    }
                    
                    reader.readAsBinaryString(f);
                }
            }
        
        },false);
    }
    
    window.onload = start;

})(window, document);